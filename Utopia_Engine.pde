//These are the only public variables, but mainly only because they are used only in this section

Screen currentScreen;
Screen mainmenu;
Screen adventure;
Screen workshop;

void setup()
{
  try
  {
    println("Attempting");
    //Ensure that global variables are initialized
    Reg.player = new PlayerCharacter();
    Reg.init();
    
    try
    {
      textFont( createFont("/data/chiller.ttf", 32) );
    }
    catch (Exception e)
    {
      println("Failed to load chiller");
    }
    Reg.currentState = Reg.STATE.MAINMENU;
    mainmenu = new MainMenu();
    currentScreen = mainmenu;
  }
  catch (Exception e)
  {
    println("Error starting game");
    println(e);
    exit();
  }
}

void draw()
{
  background(0); //Clear up previous screen
  currentScreen.update();
  currentScreen.draw();
  
  switch(Reg.currentState)
  {
    case STARTGAME:
      adventure = new AdventureScreen();
      workshop = new WorkshopScreen();
      currentScreen = adventure;
      Reg.currentState = Reg.STATE.SEARCH;
    case GOEXPLORE:
      currentScreen = adventure;
      Reg.currentState = Reg.STATE.SEARCH;
      break;
    case GOWORKSHOP:
      currentScreen = workshop;
      Reg.currentState = Reg.STATE.WORKSHOP;
      break;
    default:
      break;
  }
  
  if(Reg.mouseReleased)
  {
    Reg.mouseReleased = false;
  }
  
  if(mousePressed)
  {
    if(!Reg.mouseDown)
    {
      Reg.mousePressed = true;
    }
    else
    {
      Reg.mousePressed = false;
    }
    Reg.mouseDown = mousePressed;
  }
  else
  {
    if(Reg.mouseDown)
    {
      Reg.mouseReleased = true;
    }
    Reg.mouseDown = false;
  }
  
  
}