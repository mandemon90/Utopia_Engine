class MainMenu extends Screen
{
  
  MainMenu()
  {
    super();
    add (new Sprite(0,0, "/media/Adventure.png") );
    add( new Button(300, 100, "/media/Start.png", new Start() ) );
    add( new Button(300, 250, "/media/Exit.png", new Exit() ) );
  }
  
  void update()
  {
    content.update();
  }
  
  class Exit implements CallBack
  {
    Exit() {}
    void run() { exit(); }    
  }
  
  class Start implements CallBack
  {
    Start() {};
    void run() { Reg.currentState = Reg.STATE.STARTGAME; };
  }
  
}