class Location
{
  Location(String name, String resource, String artifact, String treasure)
  {
    encounters = new ArrayList<Enemy>();
    this.name = name;
    this.resource = new Item(Reg.ITEMTYPE.RESOURCE, resource);
    this.artifact = new Item(Reg.ITEMTYPE.ARTIFACT, artifact);
    this.treasure = new Item(Reg.ITEMTYPE.TREASURE, treasure);
    println("Created location " + this.name + " with resource " + this.resource.name + ", artifact " + this.artifact.name + " and treasure " + this.treasure.name);
  }
  
  void addEncounter(Enemy e)
  {
    encounters.add(e);
    println("Added creature " + e.name + ", level " + e.level + ", attack " + e.attack + ". hit " + e.hit);
  }
  
  Enemy getEncounter(int level)
  {
    for(Enemy e : encounters)
    {
      if(level == e.level)
      {
        return e;
      }
    }
    
    return new Enemy(0, "Generic Enemy", 1, 3); //Send generic enemy that is easy to kill
  }
  
  Item find(Reg.ITEMTYPE type)
  {
    switch(type)
    {
      case ARTIFACT:
        return artifact;
      case TREASURE:
        return treasure;
      case RESOURCE:
        return resource;
      default:
        return resource;
    }
  }
  
  
  
  private String name;
  private Item resource;
  private Item artifact;
  private Item treasure;
  ArrayList<Enemy> encounters;
}

class Enemy
{
  Enemy(int level, String name, int attack, int hit)
  {
    this.level = level;
    this.name = name;
    this.attack = attack;
    this.hit = hit;
  }
  
  
  boolean attacks(int roll)
  {    
     if (roll <= attack)
     {
       return true;
     }
    
    return false;
  }
  
  boolean isHit(int roll)
  {
    if(roll >= hit)
    {
      return true;
    }
    
    return false;
  }
  
  int level;
  String name;
  int attack;
  int hit;
}