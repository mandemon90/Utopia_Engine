//This is the basic class. It has its update function declared as abstract, so that other classes 
//that are derivered from it have to implement it. No instances of GraphicsOBject class alone should be
//allowed to happen.
abstract class GraphicsObject
{
  
  GraphicsObject(float x, float y)
  {
    this.x = x;
    this.y = y;
    this.visible = true;
  }
  
  //All child-classes must implement this part.
  abstract void update();
  
  //Does not actually daw anything, but remains as a refernece.
  void draw() {
    if(!visible)
    {
      return;
    }
  };
  
  void show()
  {
    visible = true;
  }
  
  void hide()
  {
    visible = false;
  }
  
  boolean isVisible()
  {
    return visible;
  }
  
  float getX()
  {
    return x;
  }
  
  float getY()
  {
    return y;
  }
  
  void setPosition(float x, float y)
  {
    this.x = x;
    this.y = y;
  }
  
  void setX(float x)
  {
    this.x = x;
  }
  
  void setY(float y)
  {
    this.y = y;
  }
  
  
  private float x;
  private float y;
  private boolean visible;
}


// This is a layer. It is used to group things together and draw them by just calling the layer, rahter than try to
// call each object individually
class GraphicsLayer extends GraphicsObject
{
  GraphicsLayer()
  {
    super(0,0);
    items = new ArrayList<GraphicsObject>();
  }
  
  //Used to clear up stuff, altough Java does it automatically
  void destroy()
  {
    items.clear();
    items = null;
  }
  
  //Implementation of update
  void update()
  {
    for(GraphicsObject i : items)
    {
      i.update();
    }
    
    this.draw();
  }
  
  //Checks if the layer is drawn, if so, then draw all its items
  void draw()
  {
    //If the entire layer is set to invisible, don't draw it
    if(!this.isVisible() )
    {
      return;
    }
    
    for(GraphicsObject i : items)
    {
      i.draw();
    }
  }
  
  //Get a list of stuff on the layer
  ArrayList<GraphicsObject> getItems()
  {
    return items;
  }
  
  
  //Addition and removal
  void add(GraphicsObject item)
  {
    items.add(item);
  }  
  void remove(GraphicsObject item)
  {
    items.remove(item);
  }
  
  private ArrayList<GraphicsObject> items;
}



//Sprite class handles almost all graphics stuff. It takes a route to spritesheet as an argument and converts that into frames.
//These frames can then be used to create and display animations
class Sprite extends GraphicsObject
{
  Sprite(float x, float y, String f)
  {
    super(x,y);
    loop = true;
    sheet = loadImage(f);
    frames = new ArrayList<PImage>();
    animations = new ArrayList<Animation>();
    frames.add(sheet);
    width = sheet.width;
    height = sheet.height;
    currentFrame = 0;
    addAnimation("Normal", 0,0);
  }
  
  Sprite(float x, float y, String f, int w, int h)
  {
    super(x,y);
    loop = true;
    sheet = loadImage(f);
    frames = new ArrayList<PImage>();
    animations = new ArrayList<Animation>();
    this.width = w;
    this.height = h;
    currentFrame = 0;
    createFrames();
  }
  
  private void createFrames()
  {
    int framesPerLine = floor(sheet.width / this.width);
    int lines = floor(sheet.height / this.height);
    
    try
    {
      for(int i = 0; i < lines; i++)
      {
        for(int j = 0; j < framesPerLine; j++)
        {
          PImage frame = sheet.get(j*width, i*height, width, height);
          frames.add(frame);
        }
      }
    }
    catch (Exception e)
    {
    }
    
  }
  
  void draw()
  {
    if( isVisible() )
    {
      try
      {
        image(frames.get(currentFrame), getX(), getY());
      }
      catch(Exception e)
      {
        println("Issue when drawing frame " + currentFrame);
        println(e);
      }
    }
  }
  
  void update()
  {
    if( !isVisible() )
    {
      return;
    }
    if(currentAnimation != null)
    {
      if(counter <= 0)
      {
        currentFrame++;
        
        if(currentFrame >= currentAnimation.getEnd())
        {
          if(loop)
          {
            currentFrame = currentAnimation.getStart();
          }
        }
        
        counter = speed;
      }
      else
      {
        counter--;
      }
    }
  }
  
  //Adds a new animation to the sprite. Checks that start is before the end and replaces old animation if there is one.
  void addAnimation(String name, int start, int end)
  {
    if(start < end)
    {
      return;
    }
    
    for(int i = 0; i < animations.size(); i++)
    {
      if(animations.get(i).name == name)
      {
        animations.get(i).changeFrames(start, end);
        return;
      }
    }
    
    //If the frames are OK but there is no animation with that name, add it to the list
    animations.add(new Animation(name, start, end));
  }
  
  //Searches for animation of that name and sets the FIRST instance of that as the current animation.
  void playAnimation(String name)
  {
    for(int i = 0; i < animations.size(); i++)
    {
      if(animations.get(i).getName() == name)
      {
        currentAnimation = animations.get(i);
        this.loop = true;
        this.speed = 20;
        this.counter = this.speed;
        return;
      }
    }
  }
  
  //Searches for animation of that name and sets the FIRST instance of that as the current animation. Also changes the value of loop
  void playAnimation(String name, boolean loop)
  {
    for(int i = 0; i < animations.size(); i++)
    {
      if(animations.get(i).getName() == name)
      {
        currentAnimation = animations.get(i);
        this.loop = loop;
        this.speed = 20;
        this.counter = this.speed;
        return;
      }
    }
  }
  
  //Searches for animation of that name and sets the FIRST instance of that as the current animation. Also changes the value of loop
  void playAnimation(String name, boolean loop, int speed)
  {
    for(int i = 0; i < animations.size(); i++)
    {
      if(animations.get(i).getName() == name)
      {
        currentAnimation = animations.get(i);
        this.loop = loop;
        this.speed = speed;
        this.counter = this.speed;
        return;
      }
    }
  }
  
  //Plays a single frame
  void playFrame(int frame)
  {
    currentFrame = frame;
    loop = false;
    currentAnimation = null;
    speed = 0;
    counter = 0;
  }
  
  int getFrame()
  {
    return currentFrame;
  }
  
  boolean mouseOver()
  {
    if(getX() < mouseX && mouseX < getX() + this.width && getY() < mouseY && mouseY < getY() + this.height)
    {
      return true;
    }
    
    return false;
  }
  
  int getWidth()
  {
    return width;
  }
  
  int getHeight()
  {
    return height;
  }
  
  boolean collision(Sprite target)
  {
    
    if(this.getX() < target.getX() + target.getWidth() )
    {
      if(this.getY() < target.getY() + target.getHeight())
      {
        return true;
      }
      if(target.getY() < this.getY() + this.getHeight())
      {
        return true;
      }
    }
    else if(target.getX() < this.getX() + this.getWidth() )
    {
      if(this.getY() < target.getY() + target.getHeight())
      {
        return true;
      }
      if(target.getY() < this.getY() + this.getHeight())
      {
        return true;
      }
    }
    
    return false;
  }
  
  private PImage sheet;
  private ArrayList<PImage> frames;
  private int width;
  private int height;
  private int currentFrame;
  private Animation currentAnimation;
  private ArrayList<Animation> animations;
  private boolean loop;
  private int speed;
  private int counter;
  
  //This class is part of sprite class, and defines animation.
  //All access to this class must be done through Sprite class
  private class Animation
  {
    Animation(String name, int start, int end)
    {
      this.name = name;
      this.start = start;
      this.end = end;
    }
    
    String getName()
    {
      return name;
    }
    
    int getStart()
    {
      return start;
    }
    
    int getEnd()
    {
      return end;
    }
    
    void changeFrames(int start, int end)
    {
      this.start = start;
      this.end = end;
    }
    
    private String name;
    private int start;
    private int end;
  }
  
}

class Text extends GraphicsObject
{
  Text(float x, float y, String message)
  {
    super(x,y);
    this.message = message;
    this.size = 32;
    this.textColor = 0;
  }
  
  Text(float x, float y, String message, int size)
  {
    super(x,y);
    this.message = message;
    this.size = size;
    this.textColor = 0;
  }
  
  //Doesn't actually do anything
  void update(){};
  
  void draw()
  {
    if(!this.isVisible() )
    {
      return;
    }
    fill(textColor);
    textSize(size);
    text(message, getX(), getY());
  }
  
  void setString(String message)
  {
    this.message = message;
  }
  
  String getString()
  {
    return message;
  }
  
  void setColor(int textColor)
  {
    this.textColor = textColor;
  }
  
  private String message;
  private int size;
  private int textColor;
}