class TextZone extends GraphicsLayer
{
  TextZone(float x, float y, String text)
  {
    super();
    add( new Sprite(x, y, "/media/TextArea.png") );
    message = new Text(x+15, y+35, text);
    add(message);
  }
  
  void setText(String text)
  {
    message.setString(text);
  }
  
  private Text message;
}