//Scene class contains basics for all content for a scene. It is separate from graphics, insteads acting as
//a parent class from main menu, game menu, etc. etc.
//We create children class that then implement the update() section.
abstract class Screen
{
  Screen()
  {
    content = new GraphicsLayer();
  }
  
  void add(GraphicsObject o)
  {
    content.add(o);
  }
  
  void remove(GraphicsObject o)
  {
    content.remove(o);
  }
  
  abstract void update();
  
  void draw()
  {
    content.draw();
  }
  
  GraphicsLayer content;
}