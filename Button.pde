interface CallBack
{
  void run();
}

class Button extends Sprite
{
  Button(float x, float y, String f, CallBack onClick)
  {
    super(x,y,f);
    this.onClick = onClick;
    timer = millis();
  }
  
  Button(float x, float y, String f, CallBack onClick, int w, int h)
  {
    super(x,y,f, w, h);
    this.onClick = onClick;
  }
  
  void update()
  {
    super.update();
    if(!isVisible())
    {
      return;
    }
    
    if(Reg.mousePressed && mouseOver() )
    {
      if(timer < millis())
      {
        onClick.run();
        timer = millis() + 100;
      }
    }
    
  }
  
  void draw()
  {
    super.draw();
  }
  
  CallBack onClick;
  private int timer;
}