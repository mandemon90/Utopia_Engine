class DiceArea extends GraphicsLayer
{
  DiceArea(float x, float y, Dice dice1, Dice dice2)
  {
    super();
    add(new Sprite(x,y, "/media/DiceArea.png") );
    result = 0;
    
    slots = new ArrayList<DiceSlot>();
    upper1 = new DiceSlot(x+70,  y+20, dice1, dice2);
    upper2 = new DiceSlot(x+140,  y+20, dice1, dice2);
    upper3 = new DiceSlot(x+210, y+20, dice1, dice2);
    
    lower1 = new DiceSlot(x+70, y+90, dice1, dice2);
    lower2 = new DiceSlot(x+140,  y+90, dice1, dice2);
    lower3 = new DiceSlot(x+210, y+90, dice1, dice2);
    
    marker =  new ResultSlot(x+5, y+180);
    result1 = new ResultSlot(x+70, y+180);
    result2 = new ResultSlot(x+140,  y+180);
    result3 = new ResultSlot(x+210, y+180);
    
    slots.add(upper1);
    slots.add(upper2);
    slots.add(upper3);
    slots.add(lower1);
    slots.add(lower2);
    slots.add(lower3);
    
    for(DiceSlot d : slots)
    {
      add(d);
    }
    
    add(marker);
    add(result1);
    add(result2);
    add(result3);
  }
  
  void update()
  {
    super.update();
    
    if(upper1.value == 0 || upper3.value == 0 || upper2.value == 0 || lower1.value == 0 || lower2.value == 0 || lower3.value == 0)
    {
      return;
    }
    
    if(!calculated)
    {
      int sum1 = 0;
      int sum2 = 0;
      result = 0;
      int value1 = 0;
      int value2 = 0;
      int value3 = 0;
      
      try
      {
        sum1 = upper1.value*100 + upper2.value*10 + upper3.value;
        sum2 = lower1.value*100 + lower2.value*10 + lower3.value;
        result = sum1 - sum2;
        println("Result: " + sum1 + " - " + sum2 + " = " + result);
      }
      catch (Exception e)
      {
        println(e);
      }
      
      boolean flipped = false;
      
      if(result < 0)
      {
        marker.playFrame(11);
        result = result*-1;
        flipped = true;
      }
      
      try
      {
        value1 = int(result / 100);
        value2 = int((result-value1*100) / 10);
        value3 = result - value1*100 - value2*10;
        println("Results: " + value1 + ", " + value2 + ", " + value3 );
      }
      catch (Exception e)
      {
        println(e);
      }
      
      result1.setValue(value1);
      result2.setValue(value2);
      result3.setValue(value3);
      
      if(flipped)
      {
        result = result*-1;
      }
      
      calculated = true;
    }
    
    
  }
  
  void reset()
  {
    for(DiceSlot d : slots)
    {
      d.reset();
    }
    
    result1.reset();
    result2.reset();
    result3.reset();
    calculated = false;
  }
  
  int getResult()
  {
    return result;
  }
  
  boolean done()
  {
    return calculated;
  }
  
  private ArrayList<DiceSlot> slots;
  
  private DiceSlot upper1;
  private DiceSlot upper2;
  private DiceSlot upper3;
  
  private DiceSlot lower1;
  private DiceSlot lower2;
  private DiceSlot lower3;
  
  private ResultSlot marker;
  private ResultSlot result1;
  private ResultSlot result2;
  private ResultSlot result3;
  
  
  private int result;
  private boolean calculated = false;
}




class DiceSlot extends Sprite
{
  DiceSlot(float x, float y, Dice dice1, Dice dice2)
  {
    super(x,y,"/media/Dice.png", 64, 64);
    playFrame(0);
    this.dice1 = dice1;
    this.dice2 = dice2;
    value = 0;
    this.active = true;
  }
  
  void update()
  {
    super.update();
    
    if(!isVisible())
    {
      return;
    }
    
    if(value == 0)
    {
      if(Reg.mouseReleased)
      {
        if(dice1.isVisible() )
        {
          checkDice(dice1);
        }
        
        if(dice2.isVisible())
        {
          checkDice(dice2);
        }
      }
    }
    
  }
  
  void checkDice(Dice d)
  {
    
    if(d.getX() < this.getX() + this.getWidth() && this.getX() < d.getX() + d.getWidth())
    {
      if(this.getY() < d.getY() + d.getHeight() && d.getY() < this.getY() + this.getHeight())
      {
        value = d.getResult();
        playFrame(d.getFrame() );
        d.setPosition(0,0);
        d.hide();
      }
    }
  }
  
  void setValue(int v)
  {
    this.value = v;
    playFrame(v);
  }
  
  void reset()
  {
    value = 0;
    playFrame(0);
  }
  
  private Dice dice1;
  private Dice dice2;
  private boolean active;
  int value;
}

class ResultSlot extends Sprite
{
  ResultSlot(float x, float y)
  {
    super(x,y,"/media/Results.png", 64, 64);
    playFrame(0);
    value = 0;
  }
  
  void update()
  {
    super.update();
    
    if(!isVisible())
    {
      return;
    }
    
  }
  
  void setValue(int v)
  {
    this.value = v;
    playFrame(v+1);
  }
  
  void reset()
  {
    value = 0;
    playFrame(0);
  }
  private boolean active;
  int value;
}