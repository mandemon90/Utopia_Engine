class PlayerCharacter
{
   PlayerCharacter()
   {
     inventory = new ArrayList<Item>();
   }
   
   void addItem(Item i)
   {
     inventory.add(i);
   }
   
   boolean doesPlayerHave(String itemName)
   {
     for(Item i : inventory)
     {
       if(i.name == itemName)
       {
         return true;
       }
     }
     
     return false;
   }
   
   void rest()
   {
     Reg.days--;
     hp = 6;
   }
   
   int hp = 6;
   ArrayList<Item> inventory;
}

class Item
{
  Item(Reg.ITEMTYPE type, String name)
  {
    this.type = type;
    this.name = name;
  }
  
  Reg.ITEMTYPE type;
  String name;
  
}