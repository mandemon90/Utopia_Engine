class WorkshopScreen extends Screen
{
  WorkshopScreen()
  {
    super();
    add(new Sprite(0,0, "/media/Workshop.png") );
    add(Reg.characterStats);
    add( new Button(700, 15, "/media/toAdventure.png", new GoExplore() ) );
    
  }
  
  void update()
  {
   content.update();
  }
  
  
  class GoExplore implements CallBack
  {
    GoExplore()
    {
    }
    
    void run()
    {
      Reg.currentState = Reg.STATE.GOEXPLORE;
    }
  }
}