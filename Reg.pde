//This class is static and acts as a global registry for stuff

static class Reg
{
  //Used to initialize all the variables
  static void init()
  {
    mouseReleased = false;
    currentState = STATE.INIT;
    days = 23;
    artifactsFound = new boolean[6];
    for(int i = 0; i < artifactsFound.length; i++)
    {
      artifactsFound[i] = false;
    }
    //player = new PlayerCharacter();
  }
  
  void changeState(STATE newState)
  {
    currentState = newState;
  }
  
  STATE getState()
  {
    return currentState;
  }
  
  void passTime()
  {
    dayCounter--;
    if(dayCounter <= 0)
    {
      dayCounter = 4;
      days--;
    }
  }
  
  
  
  enum STATE {INIT, MAINMENU, STARTGAME, GOEXPLORE, GOWORKSHOP, RUNNING, SEARCH, ENCOUNTER, RESULTS, EXPLORING, WAITING, WORKSHOP};  
  enum ITEMTYPE {TREASURE, ARTIFACT, RESOURCE};
  static private int days;
  static private STATE currentState;
  static private boolean mouseReleased;
  static private boolean mousePressed;
  static private boolean mouseDown;
  static private PlayerCharacter player;
  static private CharacterSheet characterStats;
  static boolean[] artifactsFound;
}