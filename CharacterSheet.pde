class CharacterSheet extends GraphicsLayer
{
  CharacterSheet(float x, float y)
  {
    super();
    health = new Text(x+100, y+50, "" + Reg.player.hp);
    days = new Text(x+300, y+50, "" + Reg.days);
    add(new Sprite(x,y, "/media/CharacterSheet.png") );
    add(health);
    add(days);
  }
  
  void update()
  {
    super.update();
    health.setString("" + Reg.player.hp );
    days.setString("" + Reg.days);
    
  }
  
  private Text health;
  private Text days;
}