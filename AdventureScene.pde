class AdventureScreen extends Screen
{
  AdventureScreen()
  {
    super();
    add(new Sprite(0,0, "/media/Adventure.png"));
    searchUI = new GraphicsLayer();
    exploreUI = new GraphicsLayer();
    diceResults = new GraphicsLayer();
    combatUI = new GraphicsLayer();
    turnOver = false;
    battleCounter = 0;
    add(new Button(700, 15, "/media/toWorkshop.png", new toWorkshop() ) );
    
    place = null;
    monster = null;
    locations = new ArrayList<Location>();
    
    try
    {
      XML root = loadXML("/data/locations.xml");
      println(root);
      try
      {
        XML[] list = root.getChildren();
        
        println("Number of entries: " + list.length);
        
        for(int i = 1; i < list.length-1; i+=2)
        {
          
          XML entry = list[i];
          String file = "/data/" + entry.getContent();
          println(entry);
          println(file);
          readLocation( loadXML( file ) );
        }
      }
      catch (Exception e)
      {
        println("No content");
        println(e);
      }
    }
    catch (Exception e)
    {
      println("Error when reading file");
      println(e);
    }
    //searchUI.add( new Button( 412, 390, "/media/Search.png", new GoExplore("canyon", this) ) )  
    //searchUI.add( new Button( 110, 355, "/media/Search.png", new GoExplore("swamp", this) ) );
    //searchUI.add( new Button( 600, 520, "/media/Search.png", new GoExplore("maw", this) ) );
    
    dice1 = new Dice(520, 250);
    dice2 = new Dice(520, 320);
    exBoard = new DiceArea( 100, 300, dice1, dice2);
    resultBox = new TextZone (400, 300, "");
    
    exploreUI.add(new Button(500, 390, "/media/Roll.png", new RollDice(this) ));
    diceResults.add(exBoard);   
    diceResults.add(resultBox);
    exploreUI.add(dice1);
    exploreUI.add(dice2);
    okButton = new Button( 500, 450, "/media/OK.png", new Continue(this) );
    battleButton = new Button( 500, 450, "/media/Fight.png", new Fight(this) );
    
    combat1 = new Dice(190, 180);
    combat1 = new Dice(370, 180);
    //battleRoll = newutton( 250, 180, "/media/Roll.png", new RollBattleDice(this) );
    attack = new Text(190, 240, "");
    hit = new Text(370, 240, "");
    combatUI.add(combat1);
    combatUI.add(combat2);
    //combatUI.add(bateRoll);
    combatUI.add(attack);
    combatUI.add(hit);
    combatUI.hide();
    
    okButton.hide();
    battleButton.hide();
    
    exploreUI.hide();
    diceResults.hide();
    
    
    add(searchUI);
    add(diceResults);
    add(exploreUI);
    add(okButton);
    add(battleButton);
    add(combatUI);
    
    startSearch();
    
    
  }
  
  void update()
  {
    content.update();
    
    switch(Reg.currentState)
    {
      case EXPLORING:
        exploration();
        break;
      case RESULTS:
        showResults();
        break;
      case WAITING:
        break;
      case SEARCH:
        search();
        break;
      case ENCOUNTER:
        encounter();
      default:
        break;
    }
  }
  
  void explore(Location target)
  {
    println("Searching " + target.name );
    place = target;
    searchUI.hide();
    exploreUI.show();
    diceResults.show();
    resultBox.hide();
    Reg.currentState = Reg.STATE.EXPLORING;
    roll();
  }
  
  
  void roll()
  {
    dice1.show();
    dice2.show();
    dice1.setPosition(520, 250);
    dice2.setPosition(520, 320);
    dice1.animatedRoll();
    dice2.animatedRoll();
  }
  
  void battleRoll()
  {
    combat1.show();
    combat2.show();
    combat1.setPosition(190, 180);
    combat2.setPosition(370, 180);
    combat1.animatedRoll();
    combat2.animatedRoll();
    battleCounter = millis() + 1000;
  }
  
   void search()
  {
    
    
  }
  
  void startSearch()
  {
    diceResults.hide();
    exploreUI.hide();
    okButton.hide();
    battleButton.hide();
    resultBox.hide();
    resultBox.setText( "" );
    searchUI.show();
    exBoard.reset();
    Reg.currentState = Reg.STATE.SEARCH;
    monster = null;
    place = null;
  }
  
  /*
  VARIABLES
  */
  
  private ArrayList<Location> locations;
  private Dice dice1;
  private Dice dice2;
  private Dice combat1;
  private Dice combat2;
  private GraphicsLayer searchUI;
  private GraphicsLayer exploreUI;
  private GraphicsLayer diceResults;
  private GraphicsLayer combatUI;
  private DiceArea exBoard;
  private Location place;
  private TextZone resultBox;
  private Enemy monster;
  private Button okButton;
  private Button battleButton;
  private Button battleRoll;
  private Text attack;
  private Text hit;
  private boolean turnOver;
  private int battleCounter;
  
  void exploration()
  {
    if(exBoard.done())
    {
      Reg.currentState = Reg.STATE.RESULTS;
      exploreUI.hide();
      resultBox.show();
    }
  }
  
  void showResults()
  {
     diceResults.show();
     int result = exBoard.getResult();
     println("Got " + result);
     if(result > 0)
     {
       if(result <= 10)
       {
         //Artifact
         Item found = place.find(Reg.ITEMTYPE.ARTIFACT);
         resultBox.setText( "You found: \n" + found.name );
         Reg.player.addItem( found );
         okButton.show();
       }
       else if(result <= 99)
       {
         //Resource
         Item found = place.find(Reg.ITEMTYPE.RESOURCE);
         resultBox.setText( "You found: \n" + found.name );
         Reg.player.addItem(new Item(found.type, found.name) );
         okButton.show();
       }
       else
       {
         monster = place.getEncounter( getLevel(result) );
         resultBox.setText( "You have encountered \n" + monster.name );
         battleButton.show();
       }
     }
     else
     {
       monster = place.getEncounter( getLevel(result) );
       resultBox.setText( "You have encountered \n" + monster.name );
       battleButton.show();
     }
     
     Reg.currentState = Reg.STATE.WAITING;
  }
  
  void encounter()
  {
     if(!turnOver)
     {
       if(millis() < battleCounter)
       {
         if( monster.attacks(combat1.getResult() ) )
         {
           hit.setString("Suc   else
         {
           hit.se }
     }
   }
  
  void continnue()
  {
    Reg.currentState = Reg.STATE.SEARCH;
  }
  
  void beginBattle()
  {
    Reg.currentState = Reg.STATE.ENCOUNTER;
    combatUI.show();
    searchUI.hide();
    diceResults.hide();
    exploreUI.hide();
  }
  
  int getLevel(int roll)
  {
    
    if(roll < 0)
    {
      roll = roll*-1;
    }
    
    if(roll < 200)
    {
      return 1;
    }
    if(roll < 300)
    {
      return 2;
    }
    if(roll < 400)
    {
      return 3;
    }
    if(roll < 500)
    {
      return 4;
    }
    if(roll < 600)
    {
      return 5;
    }
    
    return 1;
  }
  
  void readLocation(XML root)
  {
    try
    {
    
      XML[] encounters = root.getChild("encounters").getChildren();
      String name = root.getString("name");
      String artifact = root.getChild("artifact").getContent();
      String resource = root.getChild("resource").getContent();
      String treasure = root.getChild("treasure").getContent();
      int x = root.getInt("X");
      int y = root.getInt("Y");
      Location l = new Location(name, resource, artifact, treasure);
      for(int i = 1; i < encounters.length-1; i+=2)
      {
        try
        {
          XML entry = encounters[i];
          int level = entry.getInt("level");
          String eName = entry.getChild("name").getContent();
          int attack = entry.getChild("attack").getIntContent();
          int hit = entry.getChild("hit").getIntContent();
          Enemy e = new Enemy(level, eName, attack, hit);
          l.addEncounter(e);
        }
        catch (Exception e)
        {
          println("Error when creating encounter, creation canceled");
          println(e);
        }
      }
      searchUI.add(new Button( x, y, "/media/Search.png", new GoExplore(l, this) ));
      println("Placed the button at " + x + "." + y);
      locations.add(l);
    }
    catch (Exception e)
    {
      println("Error when reading location, creation canceled");
      println(e);
    }
    
    
  }
  
  /*
  CALLBACKS
  */
  class GoExplore implements CallBack
   {
    GoExplore(Location place, AdventureScreen target)
    {
      this.place = place;
      this.target = target;
    }
    
    void run()
    {
        target.explore(place);      
    }
    
    private Location place;
    private AdventureScreen target;
    
  }
  
  class RollDice implements CallBack
  {
    RollDice(AdventureScreen target)
    {
      this.target = target;
    }
    
    void run()
    {
      target.roll();
    }
    
    private AdventureScreen target;
  }
  
  class RollBattleDice implements CallBack
  {
    RollBattleDice(AdventureScreen target)
    {
      this.target = target;
    }
    
    void run()
    {
      target.battleRoll();
    }
    
    private AdventureScreen target;
  }
  
  class Continue implements CallBack
  {
    Continue(AdventureScreen target)
    {
      this.target = target;
    }
    
    void run()
    {
      target.startSearch();
    }
    
    private AdventureScreen target;
  }
  
  class Fight implements CallBack
  {
    Fight(AdventureScreen target)
    {
      this.target = target;
    }
    
    void run()
    {
      target.beginBattle();
    }
    
    private AdventureScreen target;
  }
  
  class toWorkshop implements CallBack
  {
    toWorkshop()
    {
    }
    
    void run()
    {
      Reg.currentState = Reg.STATE.GOWORKSHOP;
    }
  }
  
  
}