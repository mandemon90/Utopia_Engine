//This is 6 sided dice
class Dice extends Sprite
{
  Dice(float x, float y)
  {
    super(x,y,"/media/Dice.png", 64, 64);
    side = 1;
    playFrame(1);
    animated = false;
  }
  
  void roll()
  {
    side = int(random(100) % 6) + 1;
    switch (side)
    {
      case 1:      
      playFrame(1);
        break;
      case 2:
      playFrame(2);
        break;
      case 3:      
      playFrame(3);
        break;
      case 4:      
      playFrame(4);
        break;
      case 5:      
      playFrame(5);
        break;
      case 6:      
      playFrame(6);
        break;
      default:      
      playFrame(0);
        break;
    }    
  }
  
  void animatedRoll()
  {
    animated = true;
    counter = 20;
  }
  
  void update()
  {
    
    super.update(); //We run sprite related updates
    
    if(!isVisible())
    {
      return;
    }
    
    if(animated) //If we are tp have an animated roll, we do animation logic here
    {
      speed--;
      if(speed <= 0)
      {
        roll();
        counter--;
        
        if(counter > 0)
        {
          speed = 40 / counter;
        }
        else
        {
          animated = false;
        }
      }
    }
    else if(Reg.mouseDown)
    {
      if(mouseOver())
      {
        setPosition(mouseX - 32, mouseY-32);
      }
    }
    
  }
  
  int getResult()
  {
    return side;
  }
  
  int side;
  boolean animated;
  int counter;
  int speed;
  
}